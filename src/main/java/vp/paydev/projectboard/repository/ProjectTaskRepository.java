package vp.paydev.projectboard.repository;

import org.springframework.stereotype.Repository;

import vp.paydev.projectboard.domain.ProjectTask;

import org.springframework.data.repository.CrudRepository;

@Repository
public interface ProjectTaskRepository extends CrudRepository<ProjectTask, Long>{
	ProjectTask getById(Long id);
}
